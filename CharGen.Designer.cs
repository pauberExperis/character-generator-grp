﻿namespace RPGCharMar
{
    partial class CharGen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_warrior = new System.Windows.Forms.Button();
            this.btn_wizard = new System.Windows.Forms.Button();
            this.btn_rogue = new System.Windows.Forms.Button();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_submit = new System.Windows.Forms.Button();
            this.list_hero = new System.Windows.Forms.ListView();
            this.btn_new = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lbl_wisdom = new System.Windows.Forms.Label();
            this.lbl_dexterity = new System.Windows.Forms.Label();
            this.lbl_constitution = new System.Windows.Forms.Label();
            this.lbl_charisma = new System.Windows.Forms.Label();
            this.lbl_intelligence = new System.Windows.Forms.Label();
            this.lbl_strength = new System.Windows.Forms.Label();
            this.lbl_armour = new System.Windows.Forms.Label();
            this.lbl_energy = new System.Windows.Forms.Label();
            this.lbl_hp = new System.Windows.Forms.Label();
            this.lbl_class = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.grp_sum = new System.Windows.Forms.GroupBox();
            this.grp_sum.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_warrior
            // 
            this.btn_warrior.Location = new System.Drawing.Point(40, 204);
            this.btn_warrior.Name = "btn_warrior";
            this.btn_warrior.Size = new System.Drawing.Size(137, 62);
            this.btn_warrior.TabIndex = 0;
            this.btn_warrior.Text = "Warrior";
            this.btn_warrior.UseVisualStyleBackColor = true;
            this.btn_warrior.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_wizard
            // 
            this.btn_wizard.Location = new System.Drawing.Point(308, 204);
            this.btn_wizard.Name = "btn_wizard";
            this.btn_wizard.Size = new System.Drawing.Size(137, 62);
            this.btn_wizard.TabIndex = 1;
            this.btn_wizard.Text = "Wizard";
            this.btn_wizard.UseVisualStyleBackColor = true;
            this.btn_wizard.Click += new System.EventHandler(this.btn_wizard_Click);
            // 
            // btn_rogue
            // 
            this.btn_rogue.Location = new System.Drawing.Point(567, 204);
            this.btn_rogue.Name = "btn_rogue";
            this.btn_rogue.Size = new System.Drawing.Size(137, 62);
            this.btn_rogue.TabIndex = 2;
            this.btn_rogue.Text = "Rogue";
            this.btn_rogue.UseVisualStyleBackColor = true;
            this.btn_rogue.Click += new System.EventHandler(this.btn_rogue_Click);
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(85, 35);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(575, 26);
            this.txt_name.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(337, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Name:";
            // 
            // btn_submit
            // 
            this.btn_submit.Location = new System.Drawing.Point(308, 340);
            this.btn_submit.Name = "btn_submit";
            this.btn_submit.Size = new System.Drawing.Size(137, 62);
            this.btn_submit.TabIndex = 5;
            this.btn_submit.Text = "Submit";
            this.btn_submit.UseVisualStyleBackColor = true;
            this.btn_submit.Click += new System.EventHandler(this.btn_submit_Click);
            // 
            // list_hero
            // 
            this.list_hero.HideSelection = false;
            this.list_hero.Location = new System.Drawing.Point(23, 13);
            this.list_hero.Name = "list_hero";
            this.list_hero.Size = new System.Drawing.Size(756, 404);
            this.list_hero.TabIndex = 6;
            this.list_hero.UseCompatibleStateImageBehavior = false;
            this.list_hero.View = System.Windows.Forms.View.Details;
            this.list_hero.Visible = false;
            // 
            // btn_new
            // 
            this.btn_new.Location = new System.Drawing.Point(591, 362);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(165, 42);
            this.btn_new.TabIndex = 7;
            this.btn_new.Text = "New Character";
            this.btn_new.UseVisualStyleBackColor = true;
            this.btn_new.Visible = false;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(38, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Class";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(38, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 20);
            this.label4.TabIndex = 10;
            this.label4.Text = "HP:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "Energy:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "Armour";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(38, 132);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 20);
            this.label7.TabIndex = 13;
            this.label7.Text = "Strength:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(38, 152);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 20);
            this.label8.TabIndex = 14;
            this.label8.Text = "Intelligence:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(38, 169);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 20);
            this.label9.TabIndex = 15;
            this.label9.Text = "Charisma";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(38, 189);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 20);
            this.label10.TabIndex = 16;
            this.label10.Text = "Constitution";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(38, 209);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 20);
            this.label11.TabIndex = 17;
            this.label11.Text = "Dexterity";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(38, 229);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(66, 20);
            this.label12.TabIndex = 18;
            this.label12.Text = "Wisdom";
            // 
            // lbl_wisdom
            // 
            this.lbl_wisdom.AutoSize = true;
            this.lbl_wisdom.Location = new System.Drawing.Point(163, 229);
            this.lbl_wisdom.Name = "lbl_wisdom";
            this.lbl_wisdom.Size = new System.Drawing.Size(66, 20);
            this.lbl_wisdom.TabIndex = 19;
            this.lbl_wisdom.Text = "Wisdom";
            // 
            // lbl_dexterity
            // 
            this.lbl_dexterity.AutoSize = true;
            this.lbl_dexterity.Location = new System.Drawing.Point(163, 213);
            this.lbl_dexterity.Name = "lbl_dexterity";
            this.lbl_dexterity.Size = new System.Drawing.Size(66, 20);
            this.lbl_dexterity.TabIndex = 20;
            this.lbl_dexterity.Text = "Wisdom";
            // 
            // lbl_constitution
            // 
            this.lbl_constitution.AutoSize = true;
            this.lbl_constitution.Location = new System.Drawing.Point(163, 193);
            this.lbl_constitution.Name = "lbl_constitution";
            this.lbl_constitution.Size = new System.Drawing.Size(66, 20);
            this.lbl_constitution.TabIndex = 21;
            this.lbl_constitution.Text = "Wisdom";
            // 
            // lbl_charisma
            // 
            this.lbl_charisma.AutoSize = true;
            this.lbl_charisma.Location = new System.Drawing.Point(163, 173);
            this.lbl_charisma.Name = "lbl_charisma";
            this.lbl_charisma.Size = new System.Drawing.Size(66, 20);
            this.lbl_charisma.TabIndex = 22;
            this.lbl_charisma.Text = "Wisdom";
            // 
            // lbl_intelligence
            // 
            this.lbl_intelligence.AutoSize = true;
            this.lbl_intelligence.Location = new System.Drawing.Point(163, 153);
            this.lbl_intelligence.Name = "lbl_intelligence";
            this.lbl_intelligence.Size = new System.Drawing.Size(66, 20);
            this.lbl_intelligence.TabIndex = 23;
            this.lbl_intelligence.Text = "Wisdom";
            // 
            // lbl_strength
            // 
            this.lbl_strength.AutoSize = true;
            this.lbl_strength.Location = new System.Drawing.Point(163, 133);
            this.lbl_strength.Name = "lbl_strength";
            this.lbl_strength.Size = new System.Drawing.Size(66, 20);
            this.lbl_strength.TabIndex = 24;
            this.lbl_strength.Text = "Wisdom";
            // 
            // lbl_armour
            // 
            this.lbl_armour.AutoSize = true;
            this.lbl_armour.Location = new System.Drawing.Point(163, 113);
            this.lbl_armour.Name = "lbl_armour";
            this.lbl_armour.Size = new System.Drawing.Size(66, 20);
            this.lbl_armour.TabIndex = 25;
            this.lbl_armour.Text = "Wisdom";
            // 
            // lbl_energy
            // 
            this.lbl_energy.AutoSize = true;
            this.lbl_energy.Location = new System.Drawing.Point(163, 93);
            this.lbl_energy.Name = "lbl_energy";
            this.lbl_energy.Size = new System.Drawing.Size(66, 20);
            this.lbl_energy.TabIndex = 26;
            this.lbl_energy.Text = "Wisdom";
            // 
            // lbl_hp
            // 
            this.lbl_hp.AutoSize = true;
            this.lbl_hp.Location = new System.Drawing.Point(163, 73);
            this.lbl_hp.Name = "lbl_hp";
            this.lbl_hp.Size = new System.Drawing.Size(66, 20);
            this.lbl_hp.TabIndex = 27;
            this.lbl_hp.Text = "Wisdom";
            // 
            // lbl_class
            // 
            this.lbl_class.AutoSize = true;
            this.lbl_class.Location = new System.Drawing.Point(163, 53);
            this.lbl_class.Name = "lbl_class";
            this.lbl_class.Size = new System.Drawing.Size(66, 20);
            this.lbl_class.TabIndex = 28;
            this.lbl_class.Text = "Wisdom";
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.Location = new System.Drawing.Point(158, 32);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(66, 20);
            this.lbl_name.TabIndex = 29;
            this.lbl_name.Text = "Wisdom";
            // 
            // grp_sum
            // 
            this.grp_sum.Controls.Add(this.lbl_name);
            this.grp_sum.Controls.Add(this.btn_new);
            this.grp_sum.Controls.Add(this.lbl_class);
            this.grp_sum.Controls.Add(this.label2);
            this.grp_sum.Controls.Add(this.lbl_hp);
            this.grp_sum.Controls.Add(this.label3);
            this.grp_sum.Controls.Add(this.lbl_energy);
            this.grp_sum.Controls.Add(this.label4);
            this.grp_sum.Controls.Add(this.lbl_armour);
            this.grp_sum.Controls.Add(this.label5);
            this.grp_sum.Controls.Add(this.lbl_strength);
            this.grp_sum.Controls.Add(this.label6);
            this.grp_sum.Controls.Add(this.lbl_intelligence);
            this.grp_sum.Controls.Add(this.label7);
            this.grp_sum.Controls.Add(this.lbl_charisma);
            this.grp_sum.Controls.Add(this.label8);
            this.grp_sum.Controls.Add(this.lbl_constitution);
            this.grp_sum.Controls.Add(this.label9);
            this.grp_sum.Controls.Add(this.lbl_dexterity);
            this.grp_sum.Controls.Add(this.label10);
            this.grp_sum.Controls.Add(this.lbl_wisdom);
            this.grp_sum.Controls.Add(this.label11);
            this.grp_sum.Controls.Add(this.label12);
            this.grp_sum.Location = new System.Drawing.Point(23, 13);
            this.grp_sum.Name = "grp_sum";
            this.grp_sum.Size = new System.Drawing.Size(756, 404);
            this.grp_sum.TabIndex = 30;
            this.grp_sum.TabStop = false;
            this.grp_sum.Text = "Character Summary";
            this.grp_sum.Visible = false;
            // 
            // CharGen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.grp_sum);
            this.Controls.Add(this.list_hero);
            this.Controls.Add(this.btn_submit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_name);
            this.Controls.Add(this.btn_rogue);
            this.Controls.Add(this.btn_wizard);
            this.Controls.Add(this.btn_warrior);
            this.Name = "CharGen";
            this.Text = "CharGen";
            this.grp_sum.ResumeLayout(false);
            this.grp_sum.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_warrior;
        private System.Windows.Forms.Button btn_wizard;
        private System.Windows.Forms.Button btn_rogue;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_submit;
        private System.Windows.Forms.ListView list_hero;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbl_wisdom;
        private System.Windows.Forms.Label lbl_dexterity;
        private System.Windows.Forms.Label lbl_constitution;
        private System.Windows.Forms.Label lbl_charisma;
        private System.Windows.Forms.Label lbl_intelligence;
        private System.Windows.Forms.Label lbl_strength;
        private System.Windows.Forms.Label lbl_armour;
        private System.Windows.Forms.Label lbl_energy;
        private System.Windows.Forms.Label lbl_hp;
        private System.Windows.Forms.Label lbl_class;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.GroupBox grp_sum;
    }
}

