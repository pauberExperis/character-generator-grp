﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPGClassLibrary;

namespace RPGCharMar
{
    public partial class CharGen : Form
    {
        public string currentClass = "";
        public CharGen()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            currentClass = btn_warrior.Text;
        }

        private void btn_wizard_Click(object sender, EventArgs e)
        {
            currentClass = btn_wizard.Text;
        }

        private void btn_rogue_Click(object sender, EventArgs e)
        {
            currentClass = btn_rogue.Text;
        }

        private void btn_submit_Click(object sender, EventArgs e)
        {
            if (txt_name.TextLength != 0) {
                switch (currentClass.ToLower())
                {
                    case "warrior":
                        RPGClassLibrary.Warrior war = new RPGClassLibrary.Warrior(txt_name.Text);
                        displayObject<RPGClassLibrary.Warrior>(war);
                        break;
                    case "rogue":
                        RPGClassLibrary.Rogue rog = new RPGClassLibrary.Rogue(txt_name.Text);
                        displayObject<RPGClassLibrary.Warrior>(rog);
                        break;
                    case "wizard":
                        RPGClassLibrary.Wizard wiz = new RPGClassLibrary.Wizard(txt_name.Text);
                        displayObject<RPGClassLibrary.Warrior>(wiz);
                        break;
                    default:
                        MessageBox.Show("You have to pick a class!");
                        break;
                }
            } else
            {
                MessageBox.Show("You have to write a name!");
            }
        }

        private void displayObject<T>(RPGClassLibrary.Character type)
        {
            string classType = type.GetType().ToString();
            classType = classType.Split('.').Last();
            lbl_armour.Text = type.ArmourRating.ToString();
            lbl_charisma.Text = type.Charisma.ToString();
            lbl_class.Text = classType;
            lbl_constitution.Text = type.Constitution.ToString();
            lbl_dexterity.Text = type.Dexterity.ToString();
            lbl_energy.Text = type.Energy.ToString();
            lbl_hp.Text = type.Hp.ToString();
            lbl_intelligence.Text = type.Intelligence.ToString();
            lbl_name.Text = type.Name;
            lbl_strength.Text = type.Strength.ToString();
            lbl_wisdom.Text = type.Wisdom.ToString();
            btn_new.Visible = true;
            grp_sum.Visible = true;
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            txt_name.Text = "";
            grp_sum.Visible = false;
            list_hero.Visible = false;
            btn_new.Visible = false;
            
        }
    }
}
